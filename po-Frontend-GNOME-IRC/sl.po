# 'smuxi/po-Frontend-GNOME-IRC/
# Copyright (C) 2016 smuxi's COPYRIGHT HOLDER
# This file is distributed under the same license as the smuxi package.
#
# Matej Urbančič <mateju@svn.gnome.org>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: smuxi master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/smuxi/issues\n"
"POT-Creation-Date: 2023-02-18 18:39+0000\n"
"PO-Revision-Date: 2023-09-05 22:54+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenščina <gnome-si@googlegroups.com>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || "
"n%100==4 ? 3 : 0);\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: Poedit 2.2.1\n"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:390
msgid "Query"
msgstr "Poizvedba"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:396
msgid "Op"
msgstr "Op"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:400
msgid "Deop"
msgstr "Deop"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:404
msgid "Voice"
msgstr "Glas"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:408
msgid "Devoice"
msgstr "Brez glasu"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:412
msgid "Kick"
msgstr "Brcni"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:416
msgid "Kick + Ban"
msgstr "Brcni in prepovej"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:420
msgid "Ban"
msgstr "Prepovej"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:424
msgid "Unban"
msgstr "Odstrani prepoved"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:430
#: ../src/Frontend-GNOME-IRC/IrcPersonChatView.cs:92
msgid "Whois"
msgstr "Whois"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:434
#: ../src/Frontend-GNOME-IRC/IrcPersonChatView.cs:96
msgid "CTCP"
msgstr "CTCP"

#: ../src/Frontend-GNOME-IRC/IrcGroupChatView.cs:443
#: ../src/Frontend-GNOME-IRC/IrcPersonChatView.cs:103
msgid "Invite to"
msgstr "Povabi"

#: ../src/Frontend-GNOME-IRC/CtcpMenu.cs:73
msgid "Ping"
msgstr "Pingni"

#: ../src/Frontend-GNOME-IRC/CtcpMenu.cs:78
msgid "Version"
msgstr "Različica"

#: ../src/Frontend-GNOME-IRC/CtcpMenu.cs:83
msgid "Time"
msgstr "Čas"

#: ../src/Frontend-GNOME-IRC/CtcpMenu.cs:88
msgid "Finger"
msgstr "Finger"

#: ../src/Frontend-GNOME-IRC/CtcpMenu.cs:93
msgid "Userinfo"
msgstr "Podatki uporabnika"
